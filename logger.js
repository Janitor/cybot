/* eslint no-console: 0 */
function logFactory (prefix) {
    return function (...args) {
        if (prefix) {
            args.unshift(`${prefix} `, args)
        }
        console.log.apply(console, args)
    }
}

module.exports = {
    log: logFactory(),
    error: logFactory('ERROR')
}
