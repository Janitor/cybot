const logger = require('../logger')
const AWS = require('aws-sdk')

const ddb = new AWS.DynamoDB()
const docClient = new AWS.DynamoDB.DocumentClient()

module.exports = {
    saveChat: function(msg) {
        let params = {
            TableName: 'Chat',
            Item: msg
        }
        return docClient.put(params).promise()
    },

    randomChat: function(username){
        let params = {
            TableName: 'Chat'
        }

        ddb.describeTable(params, function(error, data){
            if(error){
                logger.error(error)
            } else{
                logger.log(data)
            }
        })

    }
}
