const logger = require('../logger')

class Dynamo{

    constructor(AWS) {
        this.AWS = AWS
        this.dynamodb = new AWS.DynamoDB()
        this.docClient = new AWS.DynamoDB.DocumentClient();
    }

    setupTables(){
        const tableName = "Chat"
        this.dynamodb.listTables({})
            .promise()
            .then((data) => {
                const exists = data.TableNames
                    .filter(name => {
                        return name === tableName;
                    })
                    .length > 0;
                if (exists) {
                    return Promise.resolve();
                }
                else {
                    let params = require('./tables')[0]
                    return this.dynamodb.createTable(params).promise();
                }
            });
    }

    writeData(table, data){
        let params = {
            TableName: table,
            Item: data
        }

        logger.log('WRITING: ', data)

        return this.docClient.put(params, function(err, data){
            if(err){
                logger.log('dynamoDB error on insert:', JSON.stringify(err))
            } else{
                logger.log('dynamoDB ' + table + 'inserted')
            }
        })
    }

    getUserChat(username){
        let params = {
            TableName: 'Chat',
            KeyConditionExpression: "#username = :username",
            ExpressionAttributeNames: {
                "#username": "username"
            },
            ExpressionAttributeValues: {
                ":username": username
            }
        }

        return this.docClient.query(params, function (err, data){
            if (err) {
                logger.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            } else {
                logger.log("Query succeeded.")
                logger.log(data.Items)
            }
        });

    }
}

module.exports = Dynamo