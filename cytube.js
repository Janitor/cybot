const request = require('request')
const config = require('./config')


module.exports = {
    getUrl: function (callback) {
        const url = `${config.server}/socketconfig/${config.channel}.json`
        request(url, function (error, response) {
            let payload = null
            const errorMsg = arguments.callee
            if (error) {
                return callback(error)
            }

            if (!response || !response.body) {
                return callback(`${arguments.callee}: no response found`)
            }

            try {
                payload = JSON.parse(response.body)
            } catch (e) {
                return callback(`${errorMsg}: ${JSON.stringify(e)}`)
            }

            if (!payload || !payload.servers || !payload.servers.length) {
                return callback(`${errorMsg}: unable to process payload`)
            }

            const server = payload.servers.find(el => {
                return el.secure === true
            })

            if(!server || !server.url){
                return callback(`${errorMsg}: could not find secure server`)
            }

            return callback(null, server.url)
        })
    }
}