#!/usr/bin/env bash

StackName=$1
BucketName=cybot-client-infra

DeployVPC="vpc-3041d84a"
DeploySubnets="subnet-b3054cef"
CreateService=true

# Ensure required arguments are present
if [ "$StackName" == "" ]; then
    echo "Stack name argument is required"
    exit 1
fi

if [ "$BucketName" == "" ]; then
    echo "Bucket name argument is required"
    exit 1
fi

# If this is not a fresh deployment, grab the current image from the latest task definition,
# so we don't accidentally deploy "latest"
EcrImage=$(aws ecs describe-task-definition --task-definition ${StackName} | jq -r .taskDefinition.containerDefinitions[].image)
EcrImageTag=$(echo ${EcrImage} | cut -d':' -f 2)
EcrImageTag=${EcrImageTag:-latest}

# Package and deploy
aws cloudformation package \
--template-file cybot.yml \
--s3-bucket ${BucketName} \
--output-template-file packaged-${StackName}.yml

aws cloudformation deploy \
--stack-name ${StackName} \
--template-file packaged-${StackName}.yml \
--parameter-overrides \
"EcrImageTag=${EcrImageTag}" \
"ServiceName=${StackName}" \
"DeployVPC=${DeployVPC}" \
"DeploySubnets=${DeploySubnets}" \
"CreateService=${CreateService}" \
--s3-bucket ${BucketName} \
--capabilities CAPABILITY_IAM
