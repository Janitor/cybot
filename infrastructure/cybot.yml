AWSTemplateFormatVersion: '2010-09-09'
Description: Template describing the project resources

Parameters:
  DeployVPC:
    Description: Pre-existing VPC to deploy application into
    Type: AWS::EC2::VPC::Id
    Default: 'vpc-c730dcbd'

  DeploySubnets:
    Description: The VPC Subnets to deploy into
    Type: List<AWS::EC2::Subnet::Id>
    Default: 'subnet-73ef4b2f,subnet-67af0b00'

  ServiceName:
    Type: String
    Description: Name of the ECS Service and Container names
    Default: 'cybot'

  CreateService:
    Type: String
    Description: Flag to optionally create the ECS Service. This should be set to 'false' until an ECR image is published.
    AllowedValues:
    - 'true'
    - 'false'
    Default: 'false'

  EcrImageTag:
    Type: String
    Description: ECR Image tag to deploy
    Default: latest

Resources:

  SharedResources:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: 'shared-resources.yml'
      Parameters:
        ServiceName: !Ref ServiceName
      Tags:
        -
          Key: 'ServiceName'
          Value: !Ref ServiceName

  EcsCluster:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: 'ecs-cluster.yml'
      Tags:
        -
          Key: 'ServiceName'
          Value: !Ref ServiceName

  EcsService:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: 'ecs-service.yml'
      Parameters:
        CreateService: !Ref CreateService
        EcsCluster: !GetAtt EcsCluster.Outputs.EcsCluster
        EcrImageUrl: !Sub '${AWS::AccountId}.dkr.ecr.${AWS::Region}.amazonaws.com/${ServiceName}:${EcrImageTag}'
        ServiceName: !Ref ServiceName
        SubnetIDs: {'Fn::Join': [',', { 'Ref': 'DeploySubnets' }]}
        VpcId: !Ref DeployVPC
      Tags:
        -
          Key: ServiceName
          Value: !Ref ServiceName

  CodePipeline:
    Type: AWS::CloudFormation::Stack
    Properties:
      TemplateURL: 'codepipeline.yml'
      Parameters:
        ArtifactBucketName: !GetAtt SharedResources.Outputs.ArtifactBucketName
        ClusterName: !GetAtt EcsCluster.Outputs.EcsCluster
        CodeCommitBranchName: master
        CodeBuildServiceRoleArn: !GetAtt SharedResources.Outputs.CodeBuildServiceRoleArn
        CodePipelineServiceRoleArn: !GetAtt SharedResources.Outputs.CodePipelineServiceRoleArn
        ServiceName: !Ref ServiceName
      Tags:
        -
          Key: ServiceName
          Value: !Ref ServiceName
