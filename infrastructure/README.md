# How to deploy infra

Currently the infrastructure consists of a git repo, ecs fargate cluster, and a cicd pipeline.

__Prereqs__

1. AWS CLI should be installed and configured, and your profile is pointing at the account you wish to deploy into.
2. You need to have 'make' installed

__Initial deployment__

1. Create an S3 bucket to hold the cloudformation templates

`aws s3 mb s3://my-bucket-name`

2. Edit `infrastructure/deploy-stack.sh`, make sure the configurable options match your environment

3. Run `make infra` to deploy the first half of the solution

4. After the solution has been deployed, push a change into your 'master' branch and confirm that a container image is published to ECR.

5. Edit `infrastructure/deploy-stack.sh` and set `CreateService=true`

6. Run `make infra` to deploy the ECS service

7. That's it, you're online!