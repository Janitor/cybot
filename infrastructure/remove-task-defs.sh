#!/bin/bash
for i in $( aws ecs list-task-definitions | jq -r .taskDefinitionArns[] | grep cybot ); do
	aws ecs deregister-task-definition --task-definition $i
done
