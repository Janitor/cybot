const cytube = require('./cytube')
const config = require('./config')
const io = require('socket.io-client')
const patch = require('socketio-wildcard')(io.Manager)
const logger = require('./logger')
const AWS = require('aws-sdk')
const chat = require('./dynamo/chat')

logger.log(typeof process.env.WQEOJGKQW)

AWS.config.update({
    region: config.aws.region,

});

const secrets = new AWS.SecretsManager()
const dynamoDB = require('./dynamo/dynamo')
const dynamo = new dynamoDB(AWS)

dynamo.setupTables()
let socket = null
let ready = false
cytube.getUrl((error, server) => {

    socket = io(server)

    socket.on('connect', () => {
        start(socket)
    })

    if (config.debug) {
        patch(socket)
        socket.on('*', function (event) {
            logger.log(event)
        })
    }

    socket.on('channelCSSJS', function () {
        ready = true
    })

    socket.on('chatMsg', function (event) {
        //'chatMsg', { username: 'zim', msg: 'test', meta: {}, time: 1558661946208 }
        handleChat(event)
    })

    socket.on('pm', function (event) {
        //'pm', { username: 'zim', msg: 'test', meta: {}, time: 1558661964804, to: 'slutbot' }
        handleChat(event)
    })

    socket.on('userLeave', function (event){
        //'userLeave', { name: 'zim' }
        //remove user from list
    })

    socket.on('addUser', function(event){
        //'addUser', { name: 'zim', rank: 4, profile: [Object], meta: [Object] }
        //add user to current userlist
    })

    function handleChat(msg) {
        if (ready) {
            if (msg.username !== config.serverchat) {
                // chat.saveChat(msg).then(function (data, error) {
                //     if(error){
                //         logger.error(error)
                //     }
                // })
            } else {
                //write to server log
            }
            if (msg.msg.startsWith('!quote')){
                let text = msg.msg
                if(text.split(' ').length > 1){
                    chat.randomChat(text.split(' ')[1])
                } else{
                    chat.randomChat()
                }

            }
            if (msg.msg.startsWith('!get')) {
                dynamo.getUserChat(msg.username)
            }
        }
    }

})


function start(socket) {
    socket.emit('initChannelCallbacks')
    socket.emit('joinChannel', { name: config.channel })
    secrets.getSecretValue({ SecretId: config.aws['secret-name'] }, function (err, data) {
        if (err) {
            logger.error('Error getting secrets: ', JSON.stringify(err), err.stack)
        } else {
            let secret = JSON.parse(data.SecretString)
            socket.emit('login', { name: secret.username, pw: secret.password })
        }
    });
}